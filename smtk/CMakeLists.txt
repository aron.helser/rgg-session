add_subdirectory(simulation)
add_subdirectory(session)

if (ENABLE_PYTHON_WRAPPING)
  add_subdirectory(pybind11)
endif()
