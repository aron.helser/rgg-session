set(module_path "rggsession/simulation")
set(build_path "${PROJECT_BINARY_DIR}/${module_path}")
set(install_path "${PYTHON_MODULEDIR}/${module_path}")

# Create and install module __init__.py
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/__init__.py.in"
  "${build_path}/__init__.py" @ONLY
  )

install(
  FILES "${build_path}/__init__.py"
  DESTINATION "${install_path}"
  )
