

set(smtkRGGPythonDataTests)

if (ENABLE_PYARC_BINDINGS)
  list(APPEND smtkRGGPythonDataTests
    exportToPyARCOp)
endif()

set(smtk_module_dir ${smtk_DIR}/${SMTK_PYTHON_MODULEDIR})
if(NOT IS_ABSOLUTE ${smtk_module_dir})
  get_filename_component(smtk_module_dir
    ${PROJECT_BINARY_DIR}/${smtk_DIR}/${SMTK_PYTHON_MODULEDIR} ABSOLUTE)
endif()

foreach (test ${smtkRGGPythonDataTests})
  add_test(NAME ${test}Py
    COMMAND "${PYTHON_EXECUTABLE}" "${CMAKE_CURRENT_SOURCE_DIR}/${test}.py"
    --data-dir=${PROJECT_SOURCE_DIR}/data
    --temp-dir=${CMAKE_BINARY_DIR}/Testing/Temporary
    )
  set_tests_properties( ${test}Py PROPERTIES
    ENVIRONMENT "PYTHONPATH=${PROJECT_BINARY_DIR}:${smtk_module_dir}:$ENV{PYTHONPATH}"
    LABELS "RGGSimulation"
    )
endforeach()
