<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">

  <Definitions>

    <AttDef Type="dif3d" BaseType="" Unique="true" Associations="">

      <ItemDefinitions>

        <Double Name="power" Label="Power (W)" NumberOfRequiredValues="1">
          <BriefDescription>power of the full core in W (doesn't include the symmetry)</BriefDescription>
          <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
        </Double>

        <String Name="geometry_type" Label="Geometry Type" NumberOfRequiredValues="1">
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="Hexagonal Infinite Lattice">hexagonal_infinite_lattice</Value>
            <Value Enum="Hexagonal Full Core">hexagonal_full_core</Value>
            <Value Enum="Hexagonal Sixth Core">hexagonal_sixth_core</Value>
            <Value Enum="Hexagonal Third Core">hexagonal_third_core</Value>
            <Value Enum="Triangular Full Core">triangular_full_core</Value>
          </DiscreteInfo>
        </String>

        <String Name="Cross Sections">

          <ChildrenDefinitions>

            <File Name="isotxs" Label="Cross Sections File" NumberOfValues="1" ShouldExist="true">
              <BriefDescription>Name of mcc3 isotxs file</BriefDescription>
            </File>

          </ChildrenDefinitions>

          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="From mcc3 Computation">previous</Value>
            </Structure>
            <Structure>
              <Value Enum="From file">fromfile</Value>
              <Items>
                <Item>isotxs</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>

        <Void Name="run_dif3d" Label="Run dif3d" Optional="true" IsEnabledByDefault="true"/>

        <Double Name="max_axial_mesh_size" Label="Maximum Axial Mesh Size (m)"
                NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="true">
          <BriefDescription>maximal axial distance between each
          calculation node used in the DIF3D model.</BriefDescription>
          <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
              <DefaultValue>.05</DefaultValue>
        </Double>

        <String Name="dif_options" Label="Options">
          <BriefDescription>Additional options</BriefDescription>
          <DetailedDescription>
            The user needs to specify variant option with polynomial
            and angular approximation and  anisotropic
            scattering. Default values are provided but higher/lower
            fidelity may be needed depending on the core analyzed.
          </DetailedDescription>

          <ChildrenDefinitions>

            <Int Name="polynomial_approx_source"
                 Label="Polynomial Approximation Source" NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
              <DefaultValue>6</DefaultValue>
            </Int>

            <Int Name="polynomial_approx_fluxes"
                 Label="Polynomial Approximation Fluxes" NumberOfRequiredValues="1">
              <RangeInfo>
                <Min Inclusive="true">1</Min>
                <Max Inclusive="true">12</Max>
              </RangeInfo>
              <DefaultValue>6</DefaultValue>
            </Int>

            <Int Name="polynomial_approx_leakages"
                 Label="Polynomial Approximation Leakages" NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>1</DefaultValue>
            </Int>

            <Int Name="angular_approx"
                 Label="Angular Approximation" NumberOfRequiredValues="1">
              <RangeInfo>
                <Min Inclusive="true">1</Min>
                <Max Inclusive="true">60</Max>
              </RangeInfo>
              <DefaultValue>3</DefaultValue>
            </Int>

            <Int Name="anisotropic_scattering_approx"
                 Label="Anisotropic Scattering Approximation" NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <RangeInfo><Max Inclusive="true">1</Max></RangeInfo>
              <DefaultValue>0.5</DefaultValue>
            </Int>

            <Void Name="omega_acceleration" Label="Omega Acceleration"
                  Optional="true" IsEnabledByDefault="true"/>

            <Int Name="hex_triangular_subdivision"
                 Label="Hexagon Triangular Subdivision" NumberOfRequiredValues="1">
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
              <DefaultValue>1</DefaultValue>
            </Int>

            <Void Name="course_mesh_rebalance" Label="Course Mesh Rebalance"
                  Optional="true" IsEnabledByDefault="false"/>

          </ChildrenDefinitions>

          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Variant Options">variant_options</Value>
              <Items>
                <Item>polynomial_approx_source</Item>
                <Item>polynomial_approx_fluxes</Item>
                <Item>polynomial_approx_leakages</Item>
                <Item>angular_approx</Item>
                <Item>anisotropic_scattering_approx</Item>
                <Item>omega_acceleration</Item>
              </Items>
            </Structure>
            <Structure>
                    <Value Enum="dif_fd_options">dif_fd_options</Value>
              <Items>
                <Item>hex_triangular_subdivision</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="dif_nod_options">dif_nod_options</Value>
              <Items>
                <Item>course_mesh_rebalance</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>

        <Group Name="rebus" Label="Rebus" AdvanceLevel="0">

          <ItemDefinitions>

            <Double Name="cycle_length" Label="Cycle Length (days)"
                    NumberOfRequiredValues="1">
              <BriefDescription>cycle length in equivalent full power days</BriefDescription>
              <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
            </Double>

            <Double Name="shutdown_time_between_cycle"
                    Label="Shutdown Time between Cycles (days)">
              <BriefDescription>shutdown time between cycles in days</BriefDescription>
              <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
            </Double>

            <Int Name="num_cycles" Optional="true" IsEnabledByDefault="true"
                 Label="Number of Cycles" NumberOfRequiredValues="1">
              <BriefDescription>number of cycles to be performed</BriefDescription>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
              <DefaultValue>1</DefaultValue>
            </Int>

            <Int Name="num_subintervals" Optional="true" IsEnabledByDefault="true"
                 Label="Number of Subintervals" NumberOfRequiredValues="1">
              <BriefDescription>number of subintervals within each cycle</BriefDescription>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
              <DefaultValue>1</DefaultValue>
            </Int>

            <Group Name="decay_chain" Label="Decay Chain">

              <ItemDefinitions>

                <String Name="list_isotopes" Label="List of Isotopes"
                        NumberOfRequiredValues="1" Extensible="True">
                </String>

                <String Name="list_lumped_elements" Label="List of Lumped Elements"
                        NumberOfRequiredValues="0" Extensible="True" Optional="true" IsEnabledByDefault="false">
                </String>

                <String Name="list_dummy_elements" Label="List of Dummy Elements"
                        NumberOfRequiredValues="0" Extensible="True" Optional="true" IsEnabledByDefault="false">
                </String>

                <File Name="decay_chain_text_file" Label="Decay Chain Text File" ShouldExist="true">
                  <BriefDescription>Name of decay chain text file</BriefDescription>
                </File>

              </ItemDefinitions>

            </Group>

          </ItemDefinitions>

        </Group>

      </ItemDefinitions>
    </AttDef>

  </Definitions>

</SMTK_AttributeSystem>
