//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME smtkRGGEditAssemblyView - UI component for Edit RGG assemblies
// .SECTION Description
// .SECTION See Also

#ifndef smtkRGGEditAssemblyView_h
#define smtkRGGEditAssemblyView_h

#include "smtk/extension/qt/qtOperationView.h"
#include "smtk/session/rgg/Assembly.h"
#include "smtk/session/rgg/plugin/Exports.h"

class QColor;
class QComboBox;
class QIcon;
class QTableWidget;
namespace smtk
{
namespace extension
{
class qtItem;
}
}
class smtkRGGEditAssemblyViewInternals;

class smtkRGGEditAssemblyView : public smtk::extension::qtBaseAttributeView
{
  Q_OBJECT

public:
  smtkRGGEditAssemblyView(const smtk::extension::OperationViewInfo& info);
  virtual ~smtkRGGEditAssemblyView() override;

  static smtk::extension::qtBaseView* createViewWidget(const smtk::extension::ViewInfo& info);

  bool displayItem(smtk::attribute::ItemPtr) override;

public slots:
  void updateUI() override {} // NB: Subclass implementation causes crashes.
  void requestModelEntityAssociation() override;
  void onShowCategory() override { this->updateAttributeData(); }
  void valueChanged(smtk::attribute::ItemPtr optype) override;

protected slots:
  virtual void requestOperation(const smtk::operation::OperationPtr& op);

  // This slot is used to indicate that the underlying attribute
  // for the operation should be checked for validity
  virtual void attributeModified();
  void onAttItemModified(smtk::extension::qtItem* item);
  void apply();

  void calculatePitches();
  void launchSchemaPlanner();

protected:
  void updateAttributeData() override;
  void createWidget() override;
  // When assembly item has been modified, this function would populate the edit assembly
  // panel
  void updateEditAssemblyPanel();
  virtual void setInfoToBeDisplayed() override;

private:
  smtkRGGEditAssemblyViewInternals* Internals;
};

#endif // smtkRGGEditAssemblyView_h
