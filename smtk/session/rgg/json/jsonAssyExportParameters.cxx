//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/rgg/json/jsonAssyExportParameters.h"

namespace smtk
{
namespace session
{
namespace rgg
{
using json = nlohmann::json;

SMTKRGGSESSION_EXPORT void to_json(
  nlohmann::json& j, const AssyExportParameters& aep)
{
  // Common params
  j["geomEngine"] = aep.GeomEngine;
  // It's only used when info is on
  j["startPinId"] = aep.StartPinId;

  j["meshType"] = aep.MeshType;
  j["info"] = aep.Info;
  j["hBlock"] = aep.HBlock;
  j["geometryType"] = aep.GeometryType;
  j["geometry"] = aep.Geometry;
  j["createSideset"] = aep.CreateSideset;
  j["createFiles"] = aep.CreateFiles;
  j["saveExodus"] = aep.SaveExodus;
  j["mergeTolerance"] = aep.MergeTolerance;
  j["radialMeshSize"] = aep.RadialMeshSize;
  j["tetMeshSize"] = aep.TetMeshSize;
  j["axialMeshSize"] = aep.AxialMeshSize;
  j["edgeInterval"] = aep.EdgeInterval;
  j["meshScheme"] = aep.MeshScheme;

  // Assembly specific params
  j["rotate"] = aep.Rotate;
  j["center"] = aep.Center;
  j["section"] = aep.Section;
  j["move"] = aep.Move;
  j["neumannSet_StartId"] = aep.NeumannSet_StartId;
  j["materialSet_StartId"] = aep.MaterialSet_StartId;
  j["numSuperBlocks"] = aep.NumSuperBlocks;
  j["superblocks"] = aep.Superblocks;
  j["list_MaterialSet_StartId"] = aep.List_MaterialSet_StartId;
  j["list_NeumannSet_StartId"] = aep.List_NeumannSet_StartId;
}

SMTKRGGSESSION_EXPORT void from_json(
  const nlohmann::json& j, AssyExportParameters& aep)
{
  try
  {
    aep.GeomEngine = j.at("geomEngine");
    aep.StartPinId = j.at("startPinId");
    aep.MeshType = j.at("meshType");
    aep.Info = j.at("info");
    aep.HBlock = j.at("hBlock");
    aep.GeometryType = j.at("geometryType");
    aep.Geometry = j.at("geometry");
    aep.CreateSideset = j.at("createSideset");
    aep.CreateFiles = j.at("createFiles");
    aep.SaveExodus = j.at("saveExodus");
    aep.MergeTolerance = j.at("mergeTolerance");
    aep.RadialMeshSize = j.at("radialMeshSize");
    aep.TetMeshSize = j.at("tetMeshSize");
    aep.AxialMeshSize = j.at("axialMeshSize");
    aep.EdgeInterval = j.at("edgeInterval");
    aep.MeshScheme = j.at("meshScheme");

    // Assembly specific params
    aep.Rotate = j.at("rotate");
    aep.Center = j.at("center");
    aep.Section = j.at("section");
    aep.Move = j.at("move");
    aep.NeumannSet_StartId = j.at("neumannSet_StartId");
    aep.MaterialSet_StartId = j.at("materialSet_StartId");
    aep.NumSuperBlocks = j.at("numSuperBlocks");
    aep.Superblocks = j.at("superblocks");
    aep.List_MaterialSet_StartId = j.at("list_MaterialSet_StartId");
    aep.List_NeumannSet_StartId = j.at("list_NeumannSet_StartId");
  }
  catch (json::exception& e)
  {
    std::cerr << e.what() << "\n";
    std::cerr << "Failed to parse the AssyExportParameter in the assembly" <<std::endl;
  }

}

} // namespace rgg
} // namespace session
} // namespace smtk
