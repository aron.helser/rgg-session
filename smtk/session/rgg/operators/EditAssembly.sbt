<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the RGG "EditAssembly" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operator -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="edit assembly" Label="Model - Edit Assembly" BaseType="operation">
      <BriefDescription>Edit a RGG Assembly.</BriefDescription>
      <DetailedDescription>
        If the association is a model, then it's in creation mode. Users can use a
        duct and lay pins in the schema to define a nuclear assembly.
        If the association is an existing assembly, then it's in editing mode.
        Users can change its properties and modify the pin layout.
      </DetailedDescription>
      <AssociationsDef Name="model/assembly" NumberOfRequiredValues="1" AdvanceLevel="0">
        <Accepts>
          <Resource Name="smtk::session::rgg::Resource" Filter="model"/>
          <Resource Name="smtk::model::Resource"
                    Filter="group [ string { 'selectable' = '_rgg_assembly' } ]"/>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <Component Name="associated duct" label="associated duct" NumberOfRequiredValues="1" AdvanceLevel="0">
          <BriefDescription>A duct associated with the nuclear assembly</BriefDescription>
          <DetailedDescription>
            A json representation for the nuclear assembly. See details in jsonAssembly.h.
          </DetailedDescription>
          <Accepts>
            <Resource Name="smtk::model::Resource"
                    Filter="aux_geom [ string { 'selectable' = '_rgg_duct' } ]"/>
          </Accepts>
        </Component>

        <String Name="assembly representation" NumberOfRequiredValues="1" AdvanceLevel="11">
          <BriefDescription>A json representation for the nuclear assembly</BriefDescription>
          <DetailedDescription>
            A json representation for the nuclear assembly. See details in jsonAssembly.h.
          </DetailedDescription>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(edit assembly)" BaseType="result">
      <ItemDefinitions>
        <Component Name="tess_changed" NumberOfRequiredValues="0" Extensible="true"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
     <!--
      The customized view "Type" needs to match the plugin's VIEW_NAME:
      smtk_plugin_add_ui_view(...  VIEW_NAME smtkRGGEditAssemblyView ...)
      -->
    <View Type="smtkRGGEditAssemblyView" Title="Edit Assembly"  FilterByCategory="false"  FilterByAdvanceLevel="false" UseSelectionManager="false">
      <Description>
        A view for changing a nuclear assembly's properties and layout.
      </Description>
      <AttributeTypes>
        <Att Type="edit assembly"/>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>
