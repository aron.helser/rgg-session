<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the RGG "EditCore" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operator -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="edit core" Label="Core - Edit" BaseType="operation">
      <BriefDescription>Edit a RGG nuclear core.</BriefDescription>
      <DetailedDescription>
        Edit a RGG nuclear core's properties.
      </DetailedDescription>
      <AssociationsDef Name="Core" NumberOfRequiredValues="1" AdvanceLevel="0">
        <Accepts>
          <!-- Edit an existing rgg core -->
          <Resource Name="smtk::model::Resource"
                    Filter="group [ string { 'selectable' = '_rgg_core' } ]"/>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="core representation" NumberOfRequiredValues="1" AdvanceLevel="11">
          <BriefDescription>A json representation for the modified nuclear core</BriefDescription>
          <DetailedDescription>
            A json representation for the modified nuclear core. See details in jsonCore.h.
          </DetailedDescription>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(edit core)" BaseType="result">
      <ItemDefinitions>
          <Void Name="force camera reset" Optional="true" IsEnabledByDefault="true" AdvanceLevel="11"/>
          <Component Name="tess_changed" NumberOfRequiredValues="0" Extensible="true"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
     <!--
      The customized view "Type" needs to match the plugin's VIEW_NAME:
      smtk_plugin_add_ui_view(...  VIEW_NAME smtkRGGEditCoreView ...)
      -->
    <View Type="smtkRGGEditCoreView" Title="Edit Core"  FilterByCategory="false"  FilterByAdvanceLevel="false" UseSelectionManager="false">
      <Description>
        Change a nuclear core's properties and layout.
      </Description>
      <AttributeTypes>
        <Att Type="edit core"/>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>
