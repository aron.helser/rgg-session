//=============================================================================
// Copyright (c) Kitware, Inc.
// All rights reserved.
// See LICENSE.txt for details.
//
// This software is distributed WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef __smtk_session_rgg_ExportInp_h
#define __smtk_session_rgg_ExportInp_h

#include "smtk/session/rgg/Exports.h"

#include "smtk/operation/XMLOperation.h"

namespace smtk
{
namespace session
{
namespace rgg
{

/**\brief Export a rgg entity to inp format
  * keywords used in Assgen: https://press3.mcs.anl.gov/sigma/meshkit/rgg/assygen-input-file-keyword-definitions/
  * keywords used in CoreGen: https://press3.mcs.anl.gov/sigma/meshkit/rgg/coregen-input-file-keyword-definitions/
  */
class SMTKRGGSESSION_EXPORT ExportInp : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(smtk::session::rgg::ExportInp);
  smtkCreateMacro(ExportInp);
  smtkSharedFromThisMacro(smtk::operation::Operation);

protected:
  Result operateInternal() override;
  virtual const char* xmlDescription() const override;

  void ExportCommonInp(smtk::model::EntityRef assyG);
  void ExportAssembly(smtk::model::EntityRef assyG);
  void ExportCore(smtk::model::EntityRef coreG);

  std::string m_dir;
};

} // namespace rgg
} //namespace session
} // namespace smtk

#endif // __smtk_session_rgg_EditDuct_h
