<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the model "ReadRXFFile" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operator -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="read rxf file" Label="Model - Read RXF File" BaseType="operation">
      <BriefDescription>
        Import an XML description of rgg entities in rxf format into an empty
        model.
      </BriefDescription>
      <DetailedDescription>
        Import an XML description of rgg entities in rxf format into an empty
        model.
        This reader would decide core properties(height, thicknesses)
         based on duct info.
        Due to the limitation of xrf file, geometry type can only be decided
        when assemblies are present.
      </DetailedDescription>
      <AssociationsDef Name="model" NumberOfRequiredValues="0"
                       Extensible="true" MaxNumberOfValues="1">
        <MembershipMask>model</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <File Name="filename" NumberOfRequiredValues="1"
          ShouldExist="true"
          FileFilters="rgg XML file (*.rxf *.xml);;All files (*.*)">
        </File>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(read rxf file)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::session::rgg::Resource"/>
          </Accepts>
        </Resource>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
        <ModelEntity Name="tess_changed" NumberOfRequiredValues="0" Extensible="true"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
