//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME OperationHelper - A bunch of helper functions for rgg operations
// used on client(qt) and server(operator) side
// .SECTION Description
// .SECTION See Also

#ifndef __smtk_session_rgg_OperationHelper_h
#define __smtk_session_rgg_OperationHelper_h

#include "smtk/session/rgg/Assembly.h"
#include "smtk/session/rgg/Core.h"
#include "smtk/session/rgg/Duct.h"
#include "smtk/session/rgg/Pin.h"

#include "smtk/session/rgg/Exports.h"

#include "smtk/PublicPointerDefs.h"

namespace smtk
{
namespace session
{
namespace rgg
{

/**\brief A bunch of helper functions for rgg operations
  */
class SMTKRGGSESSION_EXPORT OperationHelper
{
public:
  static constexpr const char* const protoTypeDescription = "_rgg_prototype";

  // Use the schema info to calculate the coordinates for the underlying pins and ducts
  // as vtk3DGlyphMapper cannot create instances based on an instance
  static bool calculateEntityCoordsInCore(Core& core, smtk::model::ResourcePtr resource);

  // After all info is set, use the schema info to calculate the coordinates for the
  // underlying pins and ducts used by the assembly
  static bool calculateEntityCoordsInAssembly(Assembly& assy, bool isHex,
                                              smtk::model::ResourcePtr resource);

  // Helper function for operators to glyph the ducts
  static void glyphDucts(smtk::model::ResourcePtr& resource,
                                           smtk::attribute::ComponentItemPtr& createdItem,
                                           smtk::attribute::ComponentItemPtr& modifiedItem,
                                           smtk::attribute::ComponentItemPtr& tessChangedItem,
                                           smtk::model::Group& group,
                                           const Assembly::UuidToCoordinates& uuidToCoord,
                                           const std::string& label);

  // Helper function for operators to glyph the pins
  static void glyphPins(smtk::model::ResourcePtr& resource,
                                           smtk::attribute::ComponentItemPtr& createdItem,
                                           smtk::attribute::ComponentItemPtr& modifiedItem,
                                           smtk::attribute::ComponentItemPtr& tessChangedItem,
                                           smtk::model::Group& group,
                                           const Assembly::UuidToCoordinates& uuidToCoord,
                                           const std::string& label);

  // Create place holders for rgg pin
  static void populateChildrenInPinAux(smtk::model::AuxiliaryGeometry& pinAux,
    std::vector<smtk::model::EntityRef>& subAuxGeoms, const Pin& pin);

  // Create place holders for rgg duct
  static void populateChildrenInDuctAux(smtk::model::AuxiliaryGeometry& ductAux,
    std::vector<smtk::model::EntityRef>& subAuxgeoms, const Duct& duct);

  // Update related assemblies and core when pin/aux has changed
  static void updateRelatedAssyAndCore(smtk::model::AuxiliaryGeometry& aux,
                                           smtk::attribute::ComponentItemPtr& createdItem,
                                           smtk::attribute::ComponentItemPtr& modifiedItem,
                                           smtk::attribute::ComponentItemPtr& expungedItem,
                                           smtk::attribute::ComponentItemPtr& tessChangedItem,
                                           bool debugInfo = false);
};

} // namespace rgg
} //namespace session
} // namespace smtk

#endif // __smtk_session_rgg_OperationHelper_h
