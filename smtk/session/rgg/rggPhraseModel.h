//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_view_RGGPhraseModel_h
#define smtk_view_RGGPhraseModel_h

#include "smtk/view/PhraseModel.h"
#include "smtk/view/View.h"

#include "smtk/session/rgg/Exports.h"

#include <functional>
#include <map>

namespace smtk
{
namespace session
{
namespace rgg
{

/**\brief Present a nuclear reactor design.
  *
  * This phrase model provides custom top-level phrases
  * for pins, assemblies, cores, etc.
  */
class SMTKRGGSESSION_EXPORT rggPhraseModel : public smtk::view::PhraseModel
{
public:
  using Observer = std::function<void(smtk::view::DescriptivePhrasePtr, smtk::view::PhraseModelEvent, int, int)>;
  using Operation = smtk::operation::Operation;

  smtkTypeMacro(smtk::session::rgg::rggPhraseModel);
  smtkSuperclassMacro(smtk::view::PhraseModel);
  smtkSharedPtrCreateMacro(smtk::view::PhraseModel);
  virtual ~rggPhraseModel();

  /// Return the root phrase of the hierarchy.
  smtk::view::DescriptivePhrasePtr root() const override;

  /**\brief Create a model and configure it given a view description.
    *
    * Note that this method, unlike the version with no parameters,
    * properly initializes its subphrase generator with a reference to
    * the created model so that subphrases are properly decorated.
    */
  static smtk::view::PhraseModelPtr create(const smtk::view::View::Component& viewComp);

protected:
  rggPhraseModel();

  virtual void handleResourceEvent(const Resource& rsrc, smtk::resource::EventType event) override;
  virtual void handleCreated(const Operation& op, const Operation::Result& res,
                             const ComponentItemPtr& data) override;

  virtual void processResource(const Resource& rsrc, bool adding);
  virtual void populateRoot();

  smtk::view::DescriptivePhrasePtr m_root;
};

}
}
}

#endif
