//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_rgg_operators_EditPin_h
#define pybind_smtk_session_rgg_operators_EditPin_h

#include <pybind11/pybind11.h>

#include "smtk/session/rgg/operators/EditPin.h"

#include "smtk/operation/XMLOperation.h"

namespace py = pybind11;

PySharedPtrClass< smtk::session::rgg::EditPin, smtk::operation::XMLOperation > pybind11_init_smtk_session_rgg_EditPin(py::module &m)
{
  PySharedPtrClass< smtk::session::rgg::EditPin, smtk::operation::XMLOperation > instance(m, "EditPin");
  instance
    .def(py::init<::smtk::session::rgg::EditPin const &>())
    .def(py::init<>())
    .def("deepcopy", (smtk::session::rgg::EditPin & (smtk::session::rgg::EditPin::*)(::smtk::session::rgg::EditPin const &)) &smtk::session::rgg::EditPin::operator=)
    .def_static("create", (std::shared_ptr<smtk::session::rgg::EditPin> (*)()) &smtk::session::rgg::EditPin::create)
    .def_static("create", (std::shared_ptr<smtk::session::rgg::EditPin> (*)(::std::shared_ptr<smtk::session::rgg::EditPin> &)) &smtk::session::rgg::EditPin::create, py::arg("ref"))
    .def("shared_from_this", (std::shared_ptr<smtk::session::rgg::EditPin> (smtk::session::rgg::EditPin::*)()) &smtk::session::rgg::EditPin::shared_from_this)
    .def("shared_from_this", (std::shared_ptr<const smtk::session::rgg::EditPin> (smtk::session::rgg::EditPin::*)() const) &smtk::session::rgg::EditPin::shared_from_this)
    ;
  return instance;
}

#endif
