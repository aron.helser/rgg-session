//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_rgg_Material_h
#define pybind_smtk_session_rgg_Material_h

#include <pybind11/pybind11.h>

#include "smtk/session/rgg/Material.h"

namespace py = pybind11;

py::class_< smtk::session::rgg::Material > pybind11_init_smtk_session_rgg_Material(py::module &m)
{
  py::class_< smtk::session::rgg::Material > instance(m, "Material");
  instance
    .def(py::init<>())
    .def(py::init<::std::string const &>())
    .def(py::init<::smtk::session::rgg::Material const &>())
    .def("deepcopy", (smtk::session::rgg::Material & (smtk::session::rgg::Material::*)(::smtk::session::rgg::Material const &)) &smtk::session::rgg::Material::operator=)
    .def("name", &smtk::session::rgg::Material::name)
    .def("density", &smtk::session::rgg::Material::density)
    .def("densityType", &smtk::session::rgg::Material::densityType)
    .def("temperature", &smtk::session::rgg::Material::temperature)
    .def("thermalExpansion", &smtk::session::rgg::Material::thermalExpansion)
    .def("compositionType", &smtk::session::rgg::Material::compositionType)
    .def("numberOfComponents", &smtk::session::rgg::Material::numberOfComponents)
    .def("component", &smtk::session::rgg::Material::component, py::arg("i"))
    .def("content", &smtk::session::rgg::Material::content, py::arg("i"))
    .def("setName", &smtk::session::rgg::Material::setName, py::arg("name"))
    .def("setDensity", &smtk::session::rgg::Material::setDensity, py::arg("density"))
    .def("setDensityType", &smtk::session::rgg::Material::setDensityType, py::arg("densityType"))
    .def("setTemperature", &smtk::session::rgg::Material::setTemperature, py::arg("temperature"))
    .def("setThermalExpansion", &smtk::session::rgg::Material::setThermalExpansion, py::arg("thermExpansion"))
    .def("setCompositionType", &smtk::session::rgg::Material::setCompositionType, py::arg("compType"))
    .def("addComponent", &smtk::session::rgg::Material::addComponent, py::arg("comp"))
    .def("addContent", &smtk::session::rgg::Material::addContent, py::arg("content"))
    .def("atomicMass", &smtk::session::rgg::Material::atomicMass, py::arg("i"))
    .def("numberDensity", &smtk::session::rgg::Material::numberDensity, py::arg("i"))
    .def("massDensity", &smtk::session::rgg::Material::massDensity)
    .def_property_readonly_static("label", [](py::object){ return std::string(smtk::session::rgg::Material::label);})
    ;
  return instance;
}

#endif
