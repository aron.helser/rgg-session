//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_session_rgg_meshkit_CoreExportParameters_h
#define __smtk_session_rgg_meshkit_CoreExportParameters_h

#include "smtk/model/FloatData.h"
#include "smtk/model/IntegerData.h"
#include "smtk/session/rgg/Exports.h"

#include <tuple>
#include <set>
#include <string>


namespace smtk
{
namespace session
{
namespace rgg
{

/**\brief Core export parameters used by meshkit
  * Explananation of keyword used by coregen:
  * https://press3.mcs.anl.gov/sigma/meshkit/rgg/coregen-input-file-keyword-definitions/
  */
struct SMTKRGGSESSION_EXPORT CoreExportParameters
{
  // a string used to fetch json rep from string property
  static constexpr const char* const propDescription = "export_parameters";

  CoreExportParameters();

  std::string Geometry;
  std::string GeometryType; // Hexagonal or Rectangular
  std::string GeomEngine;
  std::string Extrude;
  double MergeTolerance;
  std::vector<std::string> NeumannSet;
  std::string OutputFileName;
  // Currently smtk does not support shell generation.
  // Instead, shell is provided by the user (filename, pathToFile)
  std::pair<std::string, std::string> BackgroundInfo;
  std::string ProblemType;
  std::string SaveParallel;
  std::string Info;
  std::string MeshINFO;
  std::string Symmetry;

  // Params which must be placed in each inp file
private:
  void init();
};

} // namespace rgg
} // namespace session
} // namespace smtk

#endif // __smtk_session_rgg_meshkit_AssyExportParameters_h
