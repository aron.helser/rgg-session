# ==============================================================================
# Source
# ==============================================================================
find_package(Qt5 REQUIRED COMPONENTS Core Widgets OpenGL)
include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})

# Source files
# ------------
set(HEADERS
  Nuclide.h
  NuclideTable.h
  NuclideTableView.h)

set(SOURCE
  NuclideTable.cxx
  Nuclide.cxx
  NuclideTableView.cxx)

# ==============================================================================
# Application
# ==============================================================================

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

add_library(smtkQtRGGNuclides SHARED ${SOURCE} NuclideResources.qrc)

set_target_properties(smtkQtRGGNuclides PROPERTIES AUTOMOC TRUE)
target_link_libraries(smtkQtRGGNuclides
  LINK_PUBLIC
  Qt5::Core
  Qt5::Widgets
  Qt5::OpenGL
)

target_include_directories(smtkQtRGGNuclides PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
)

generate_export_header(smtkQtRGGNuclides EXPORT_FILE_NAME Exports.h)

smtk_get_kit_name(name dir_prefix)

# Install the header files
install(
  FILES
  ${HEADERS}
    ${CMAKE_CURRENT_BINARY_DIR}/Exports.h
  DESTINATION
    ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})

# Install the library and exports
install(
  TARGETS smtkQtRGGNuclides
  EXPORT  ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})
