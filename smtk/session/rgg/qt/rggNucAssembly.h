//=============================================================================
// Copyright (c) Kitware, Inc.
// All rights reserved.
// See LICENSE.txt for details.
//
// This software is distributed WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the above copyright notice for more information.
//=============================================================================
// .NAME rggNucAssembly - Represents an assembly for schema planner.
// Assemblies are composed of pin cells (cmbNucPinCell) and the surrounding ducting.
// Assemblies are grouped together into cores (rggNucCore).
// .SECTION Description
// .SECTION See Also

#ifndef __smtk_session_rgg_qt_rggNucAssembly_h
#define __smtk_session_rgg_qt_rggNucAssembly_h

#include "smtk/model/Resource.h"
#include "smtk/session/rgg/Assembly.h"
#include "smtk/session/rgg/qt/rggLatticeContainer.h"

#include "smtk/session/rgg/Exports.h"


// Represents a nuclear assembly in schema planner. All assembly details are stored in m_assy
class SMTKQTRGGSESSION_EXPORT rggNucAssembly : public rggLatticeContainer
{
  using rggAssy = smtk::session::rgg::Assembly;
public:
  rggNucAssembly(smtk::model::EntityRef entity);
  ~rggNucAssembly() override;
  virtual QString extractLabel(QString const&) override;
  // Create a list with pair<${name}(${label}, entity)>
  virtual void fillList(std::vector<std::pair<QString, smtk::model::EntityRef> >& l) override;
  virtual smtk::model::EntityRef getFromLabel(const QString& label) override;
  virtual bool IsHexType() override;
  virtual void calculateExtraTranslation(double& transX, double& transY) override;
  virtual void calculateTranslation(double& transX, double& transY) override;
  virtual void setUpdateUsed() override;
  virtual void getRadius(double& ri, double& rj) const override;

  // This function would use the assy to populate the rggNucAssembly
  void resetBySMTKAssembly(const smtk::model::EntityRef& assy);

  std::string name() const;
  std::string label() const;
  const smtk::model::FloatList& color() const;
  // Hex core only uses pitch0
  const std::pair<double, double>& pitch() const;
  // Hex core only uses size0
  const std::pair<int, int>& latticeSize() const;
  const bool centerPin() const;

  void setAssyDuct(smtk::model::EntityRef duct);
  smtk::model::EntityRef const& getAssyDuct() const;
  smtk::model::EntityRef getAssyDuct();

protected:
  smtk::model::ResourcePtr m_resource;
  rggAssy m_assy;
};

#endif
