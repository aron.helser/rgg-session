# A contract file for testing the RGG session within SMTK.

cmake_minimum_required(VERSION 3.8.2)
project(rgg-session)

include(ExternalProject)

ExternalProject_Add(rgg-session
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/rgg-session"
  GIT_TAG "origin/master"
  PREFIX plugin
  STAMP_DIR plugin/stamp
  SOURCE_DIR plugin/src
  BINARY_DIR plugin/build
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DENABLE_PYTHON_WRAPPING=ON
    -DENABLE_TESTING=ON
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -Dsmtk_DIR=${smtk_DIR}
    ${response_file}
  INSTALL_COMMAND ""
  TEST_BEFORE_INSTALL True
)
